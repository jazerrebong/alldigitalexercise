//
//  CustomTableViewCell.h
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/21/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

+ (NSString *)reuseIdentifier;


@property (nonatomic, weak) IBOutlet UILabel *movieTitle;
@property (nonatomic, weak) IBOutlet UILabel *rating;
@property (nonatomic, weak) IBOutlet UIImageView *artWork;


@end
