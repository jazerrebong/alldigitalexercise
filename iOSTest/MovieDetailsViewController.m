//
//  MovieDetailsViewController.m
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/21/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import "MovieDetailsViewController.h"


@interface MovieDetailsViewController ()
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@end

@implementation MovieDetailsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"";
    }
    return self;
}

- (APIResultsforMovieMedia *)resultsObject
{
    if(!_resultsObject) _resultsObject = [[APIResultsforMovieMedia alloc] init];
    
    return _resultsObject;
}

-(IBAction)watchPreviewPressed:(id)sender{
    NSLog(@"PREVIEW = %@",self.resultsObject.previewUrl);
    
/*
 Cannot play videos because of DRM
 */
    
//    MPMoviePlayerViewController *player = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:self.resultsObject.previewUrl]];
//  
//    [self presentMoviePlayerViewControllerAnimated:player];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: self.resultsObject.previewUrl]];
    
}

-(IBAction)viewInItunesPressed:(id)sender{
    NSLog(@"TRACKVIEWURL = %@",self.resultsObject.trackViewUrl);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: self.resultsObject.trackViewUrl]];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.titleLabel.text = self.resultsObject.trackName;
    self.ratingLabel.text = self.resultsObject.contentAdvisoryRating;
    self.longDescription.text = self.resultsObject.longDescription;

    NSString *priceStr = [[NSString alloc] initWithFormat:@"%d %@", self.resultsObject.collectionPrice, self.resultsObject.currency ];
   
    self.priceLabel.text = priceStr;
    
    
    
   //Format Date
    NSDateFormatter *dformat = [[NSDateFormatter alloc]init];
    
    [dformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssz"];
    
    NSDate *date = [dformat dateFromString:self.resultsObject.releaseDate];
    
    [dformat setDateFormat:@"MM/dd/yyyy"];
    
    NSString *strdate = [dformat stringFromDate:date];
   ///
    
    NSString *dateString = [[NSString alloc]initWithFormat:@"Release Date: %@",strdate];
    
    self.releaseDateLabel.text = dateString;
    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.resultsObject.artworkUrl100]];
    
    UIImage* image = [[UIImage alloc] initWithData:imageData];
    
    self.artWork.image = image;

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    if(UIInterfaceOrientationIsLandscape(interfaceOrientation))
    {
        [self.scrollView setFrame:CGRectMake(0,0,screenRect.size.width,screenRect.size.height)];
        [self.scrollView setContentSize:CGSizeMake(screenRect.size.width,460)];
        
    }
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
