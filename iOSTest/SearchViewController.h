//
//  SearchViewController.h
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/20/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ITunesSearchAPIObject.h"

@interface SearchViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) NSDictionary *jsonData;
@property (strong, nonatomic) NSMutableArray *jsonDataArray;

@property (strong, nonatomic) ITunesSearchAPIObject *itunesSearchJSON;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@end
