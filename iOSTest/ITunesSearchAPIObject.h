//
//  ITunesSearchAPIObject.h
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/21/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITunesSearchAPIObject : NSObject <NSCopying>

- (id) initWithDictionary: (NSDictionary *)dict;

+ (id) userWithDictionary: (NSDictionary *)dict;

@property (nonatomic, assign) int resultCount;
@property (nonatomic, strong) NSMutableArray *results;


@end
