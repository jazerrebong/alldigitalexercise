//
//  AppDelegate.h
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/20/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic)UINavigationController *navigationViewController;
+(AppDelegate *)sharedAppDelegate;
@end
