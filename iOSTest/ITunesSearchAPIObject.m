//
//  ITunesSearchAPIObject.m
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/21/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import "ITunesSearchAPIObject.h"
#import "APIResultsforMovieMedia.h"

@interface ITunesSearchAPIObject ()

-(BOOL)setAttributesFromDictionary:(NSDictionary *)dict;
@property (nonatomic, strong) APIResultsforMovieMedia *apiResultsforMovieMedia;

@end

@implementation ITunesSearchAPIObject
@synthesize resultCount = _resultCount, results = _results;

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (!self)
        return nil;
    
    BOOL parsedOK = [self setAttributesFromDictionary:dict];
    if (!parsedOK) {
        return nil;
    }
    
    return self;
}

+ (id)userWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (id)copyWithZone:(NSZone *)zone
{
    
    ITunesSearchAPIObject *iTunesSearchAPIObjectcopy = [[ITunesSearchAPIObject alloc] init];
    iTunesSearchAPIObjectcopy.resultCount = self.resultCount;
    iTunesSearchAPIObjectcopy.results = self.results;
    
    
    return iTunesSearchAPIObjectcopy;
}

-(APIResultsforMovieMedia *)apiResultsforMovieMedia{
    if (!_apiResultsforMovieMedia) _apiResultsforMovieMedia = [[APIResultsforMovieMedia alloc] init];
    
    return _apiResultsforMovieMedia;
}

-(NSMutableArray *)results{
    if (!_results) _results = [[NSMutableArray alloc]init];
        
    return _results;
}


-(BOOL)setAttributesFromDictionary:(NSDictionary *)dict
{
   
    
    if (![[dict objectForKey:@"resultCount"] isEqual: [NSNull null]]) {
        self.resultCount = [[dict objectForKey:@"resultCount"] integerValue];
    }
    else
        self.resultCount = 0;
    
    if (![[dict objectForKey:@"results"] isEqual: [NSNull null]]) {
       //self.results = [dict objectForKey:@"results"];
        
         for (NSDictionary *custdict in [dict objectForKey:@"results"])
         {
             self.apiResultsforMovieMedia = [[APIResultsforMovieMedia alloc] initWithDictionary:custdict];
             [self.results addObject:self.apiResultsforMovieMedia];
         }
        }
    else
        self.results = nil;

    
    
    
   
    
    BOOL success = (self.resultCount);
    return success;
}


@end
