//
//  SearchViewController.m
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/20/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import "SearchViewController.h"
#import "ITunesSearchAPIObject.h"
#import "SearchResultsViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define ENDPOINTMOVIESTRING @"https://itunes.apple.com/search?media=movie&term="

@interface SearchViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchField;

@end

@implementation SearchViewController
@synthesize jsonData,jsonDataArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"iTunes Movie Search";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
}

- (IBAction)searchButtonPressed:(id)sender {
    [self.searchButton  setEnabled:NO];
    if(self.searchField.text.length<1)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" 
                                                       message:@"Please type in a Movie Title" 
                                                      delegate:self 
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles: nil];
        [alert show];
    }
    else{
        [self.activityIndicator startAnimating];

        
        [self.searchField resignFirstResponder];
       
    
    NSString *searchQuery = [[NSString alloc] initWithFormat:@"%@%@",ENDPOINTMOVIESTRING,self.searchField.text ];
    
    NSString* encodedUrl = [searchQuery stringByAddingPercentEscapesUsingEncoding:
                            NSASCIIStringEncoding];
 
    NSURL *searchQueryUrl = [NSURL URLWithString:encodedUrl];
       
            [self.activityIndicator startAnimating];
     
    
    dispatch_async(kBgQueue, ^{
        
        NSData* data = [NSData dataWithContentsOfURL:
                        searchQueryUrl];
        if(data)
        {
            [self performSelectorOnMainThread:@selector(fetchedData:)withObject:data waitUntilDone:YES];
           
        }
        
        else
        {
            NSLog(@"No Results");
        }
    });

    }
}

- (ITunesSearchAPIObject *)itunesSearchJSON
{
    if (!_itunesSearchJSON) _itunesSearchJSON = [[ITunesSearchAPIObject alloc] init];
    
    return _itunesSearchJSON;
        
}


- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    NSDictionary *jsonDictionary = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
        
        self.itunesSearchJSON = [[ITunesSearchAPIObject alloc] initWithDictionary:jsonDictionary];
        
    
    
    [self.searchButton  setEnabled:YES];
    [self.activityIndicator stopAnimating];
    if (self.itunesSearchJSON.resultCount <1) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert"
                                                       message:@"No Matches found"
                                                      delegate:self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles: nil];
        [alert show];

    }
    else{
    
   
    SearchResultsViewController *searchResultsViewController = [[SearchResultsViewController alloc]init];
    searchResultsViewController.responseFromQueryArray = [self.itunesSearchJSON.results mutableCopy];
    [self.navigationController pushViewController:searchResultsViewController animated:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self searchButtonPressed:self];
    return NO;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
