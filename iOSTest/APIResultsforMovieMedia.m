//
//  APIResultsforMovieMedia.m
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/21/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import "APIResultsforMovieMedia.h"
@interface APIResultsforMovieMedia ()

-(BOOL)setAttributesFromDictionary:(NSDictionary *)dict;

@end


@implementation APIResultsforMovieMedia
@synthesize artistName,artworkUrl100,artworkUrl30,artworkUrl60,collectionExplicitness,collectionPrice,contentAdvisoryRating,country,currency,kind,longDescription,previewUrl,primaryGenreName,radioStationUrl,releaseDate,shortDescription,trackCensoredName,trackExplicitness,trackId,trackName,trackPrice,trackTimeMillis,trackViewUrl,wrapperType;


- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (!self)
        return nil;
    
    BOOL parsedOK = [self setAttributesFromDictionary:dict];
    if (!parsedOK) {
        return nil;
    }
    
    return self;
}

+ (id)userWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (id)copyWithZone:(NSZone *)zone
{
    
    APIResultsforMovieMedia *apiResultsforMovieMedia = [[APIResultsforMovieMedia alloc] init];
    apiResultsforMovieMedia.wrapperType = self.wrapperType;
    apiResultsforMovieMedia.artistName = self.artistName;
    apiResultsforMovieMedia.artworkUrl100 = self.artworkUrl100;
    apiResultsforMovieMedia.artworkUrl30 = self.artworkUrl30;
    apiResultsforMovieMedia.artworkUrl60 = self.artworkUrl60;
    apiResultsforMovieMedia.collectionExplicitness = self.collectionExplicitness;
    apiResultsforMovieMedia.collectionPrice = self.collectionPrice;
    apiResultsforMovieMedia.contentAdvisoryRating = self.contentAdvisoryRating;
    apiResultsforMovieMedia.country = self.country;
    apiResultsforMovieMedia.currency = self.currency;
    apiResultsforMovieMedia.kind = self.kind;
    apiResultsforMovieMedia.longDescription = self.longDescription;
    apiResultsforMovieMedia.previewUrl = self.previewUrl;
    apiResultsforMovieMedia.primaryGenreName = self.primaryGenreName;
    apiResultsforMovieMedia.radioStationUrl = self.radioStationUrl;
    apiResultsforMovieMedia.releaseDate = self.releaseDate;
    apiResultsforMovieMedia.shortDescription = self.shortDescription;
    apiResultsforMovieMedia.trackViewUrl = self.trackViewUrl;
    apiResultsforMovieMedia.trackTimeMillis = self.trackTimeMillis;
    apiResultsforMovieMedia.trackPrice = self.trackPrice;
    apiResultsforMovieMedia.trackId = self.trackId;
    apiResultsforMovieMedia.trackViewUrl = self.trackViewUrl;
    apiResultsforMovieMedia.trackCensoredName = self.trackCensoredName;
    apiResultsforMovieMedia.trackExplicitness = self.trackExplicitness;
    
    
    
    return apiResultsforMovieMedia;
}


-(BOOL)setAttributesFromDictionary:(NSDictionary *)dict
{
    
    
    if (![[dict objectForKey:@"wrapperType"] isEqual: [NSNull null]]) {
        self.wrapperType = [dict objectForKey:@"wrapperType"];
    }
    else
        self.wrapperType = nil;
    
    if (![[dict objectForKey:@"artistName"] isEqual: [NSNull null]]) {
        self.artistName = [dict objectForKey:@"artistName"];
    }
    else
        self.artistName = nil;
    
    if (![[dict objectForKey:@"artworkUrl60"] isEqual: [NSNull null]]) {
        self.artworkUrl60 = [dict objectForKey:@"artworkUrl60"];
    }
    else
        self.artworkUrl60 = nil;
    
    if (![[dict objectForKey:@"artworkUrl100"] isEqual: [NSNull null]]) {
        self.artworkUrl100 = [dict objectForKey:@"artworkUrl100"];
    }
    else
        self.artworkUrl100 = nil;
    
    if (![[dict objectForKey:@"artworkUrl30"] isEqual: [NSNull null]]) {
        self.artworkUrl30 = [dict objectForKey:@"artworkUrl30"];
    }
    else
        self.artworkUrl30 = nil;
    
    if (![[dict objectForKey:@"collectionExplicitness"] isEqual: [NSNull null]]) {
        self.collectionExplicitness = [dict objectForKey:@"collectionExplicitness"];
    }
    else
        self.collectionExplicitness = nil;
    
    if (![[dict objectForKey:@"collectionPrice"] isEqual: [NSNull null]]) {
        self.collectionPrice = [[dict objectForKey:@"collectionPrice"] integerValue];
    }
    else
        self.collectionPrice = 0;
    
    if (![[dict objectForKey:@"contentAdvisoryRating"] isEqual: [NSNull null]]) {
        self.contentAdvisoryRating = [dict objectForKey:@"contentAdvisoryRating"];
    }
    else
        self.contentAdvisoryRating = nil;

    if (![[dict objectForKey:@"country"] isEqual: [NSNull null]]) {
        self.country = [dict objectForKey:@"country"];
    }
    else
        self.country = nil;
    
    if (![[dict objectForKey:@"currency"] isEqual: [NSNull null]]) {
        self.currency = [dict objectForKey:@"currency"];
    }
    else
        self.currency = nil;
    
    if (![[dict objectForKey:@"kind"] isEqual: [NSNull null]]) {
        self.kind = [dict objectForKey:@"kind"];
    }
    else
        self.kind = nil;
    
    
    if (![[dict objectForKey:@"longDescription"] isEqual: [NSNull null]]) {
        self.longDescription = [dict objectForKey:@"longDescription"];
    }
    else
        self.longDescription = nil;
    
    if (![[dict objectForKey:@"previewUrl"] isEqual: [NSNull null]]) {
        self.previewUrl = [dict objectForKey:@"previewUrl"];
    }
    else
        self.previewUrl = nil;
    

    if (![[dict objectForKey:@"primaryGenreName"] isEqual: [NSNull null]]) {
        self.primaryGenreName = [dict objectForKey:@"primaryGenreName"];
    }
    else
        self.primaryGenreName = nil;
    
    
    if (![[dict objectForKey:@"radioStationUrl"] isEqual: [NSNull null]]) {
        self.radioStationUrl = [dict objectForKey:@"radioStationUrl"];
    }
    else
        self.radioStationUrl = nil;
    
    if (![[dict objectForKey:@"releaseDate"] isEqual: [NSNull null]]) {
        self.releaseDate = [dict objectForKey:@"releaseDate"];
    }
    else
        self.releaseDate = nil;
    
    if (![[dict objectForKey:@"shortDescription"] isEqual: [NSNull null]]) {
        self.shortDescription = [dict objectForKey:@"shortDescription"];
    }
    else
        self.shortDescription = nil;
    
    if (![[dict objectForKey:@"trackCensoredName"] isEqual: [NSNull null]]) {
        self.trackCensoredName = [dict objectForKey:@"trackCensoredName"];
    }
    else
        self.trackCensoredName = nil;
    
    if (![[dict objectForKey:@"trackExplicitness"] isEqual: [NSNull null]]) {
        self.trackExplicitness = [dict objectForKey:@"trackExplicitness"];
    }
    else
        self.trackExplicitness = nil;
    
    if (![[dict objectForKey:@"trackId"] isEqual: [NSNull null]]) {
        self.trackId = [[dict objectForKey:@"trackId"] integerValue];
    }
    else
        self.trackId = 0;
    
    if (![[dict objectForKey:@"trackName"] isEqual: [NSNull null]]) {
        self.trackName = [dict objectForKey:@"trackName"];
    }
    else
        self.trackName = nil;
    
    if (![[dict objectForKey:@"trackPrice"] isEqual: [NSNull null]]) {
        self.trackPrice = [[dict objectForKey:@"trackPrice"]integerValue];
    }
    else
        self.trackPrice = 0;
    
    if (![[dict objectForKey:@"trackTimeMillis"] isEqual: [NSNull null]]) {
        self.trackTimeMillis = [[dict objectForKey:@"trackTimeMillis"]integerValue];
    }
    else
        self.trackTimeMillis = 0;
    
    if (![[dict objectForKey:@"trackViewUrl"] isEqual: [NSNull null]]) {
        self.trackViewUrl = [dict objectForKey:@"trackViewUrl"];
    }
    else
        self.trackViewUrl = nil;

    
    





    
    
    
    
    
    BOOL success = (self.wrapperType != nil);
    return success;
}




@end
