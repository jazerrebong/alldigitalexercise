//
//  MovieDetailsViewController.h
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/21/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIResultsforMovieMedia.h"
#import <MediaPlayer/MediaPlayer.h>

@interface MovieDetailsViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *ratingLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *releaseDateLabel;
@property (nonatomic, weak) IBOutlet UITextView *longDescription;
@property (nonatomic, weak) IBOutlet UIImageView *artWork;

@property (nonatomic, strong) APIResultsforMovieMedia *resultsObject;

-(IBAction)watchPreviewPressed:(id)sender;
-(IBAction)viewInItunesPressed:(id)sender;

@end
