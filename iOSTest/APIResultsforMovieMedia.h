//
//  APIResultsforMovieMedia.h
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/21/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIResultsforMovieMedia : NSObject

- (id) initWithDictionary: (NSDictionary *)dict;

+ (id) userWithDictionary: (NSDictionary *)dict;


@property (nonatomic, strong) NSString *wrapperType;
@property (nonatomic, strong) NSString *kind;
@property (nonatomic, assign) int trackId;
@property (nonatomic, strong) NSString *artistName;
@property (nonatomic, strong) NSString *trackName;
@property (nonatomic, strong) NSString *trackCensoredName;
@property (nonatomic, strong) NSString *trackViewUrl;
@property (nonatomic, strong) NSString *previewUrl;
@property (nonatomic, strong) NSString *artworkUrl30;
@property (nonatomic, strong) NSString *artworkUrl60;
@property (nonatomic, strong) NSString *artworkUrl100;
@property (nonatomic, assign) int collectionPrice;
@property (nonatomic, assign) int trackPrice;
@property (nonatomic, strong) NSString *releaseDate;
@property (nonatomic, strong) NSString *collectionExplicitness;
@property (nonatomic, strong) NSString *trackExplicitness;
@property (nonatomic, assign) int trackTimeMillis;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *primaryGenreName;
@property (nonatomic, strong) NSString *contentAdvisoryRating;
@property (nonatomic, strong) NSString *shortDescription;
@property (nonatomic, strong) NSString *longDescription;
@property (nonatomic, strong) NSString *radioStationUrl;





@end
