//
//  SearchResultsViewController.h
//  iOSTest
//
//  Created by Ralph Jazer Rebong on 11/21/13.
//  Copyright (c) 2013 AllDigitalTest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsViewController : UITableViewController

@property (nonatomic, retain) NSMutableArray *responseFromQueryArray;

@end
